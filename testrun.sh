#!/usr/bin/env bash

gunicorn -w 4 --threads 2 -k sync -t 45 -b 0.0.0.0:8081 survey.app:app
