#!/usr/bin/env python
#
# Generate fake range data to test surveyapp
#
import redis
import random
import numpy as np
import simplejson as json
import time


SQRT_2 = 2.**0.5
HYD_RANGES = np.array([[0., 10.*SQRT_2, 10*SQRT_2, 10*SQRT_2, 10*SQRT_2],
                       [10*SQRT_2, 0., 20., 20.*SQRT_2, 20.],
                       [10*SQRT_2, 20., 0., 20., 20.*SQRT_2],
                       [10*SQRT_2, 20.*SQRT_2, 20., 0., 20.],
                       [10*SQRT_2, 20., 20.*SQRT_2, 20., 0.]])
HYD_AZ = np.array([[0, 325., 235., 145., 55.],
                   [145., 0, 190., 145., 100.],
                   [55., 10., 0, 100., 55.],
                   [325., 325., 280., 0, 10.],
                   [235., 280., 235., 190., 0]])


def next_point(interval):
    R = HYD_RANGES
    az = HYD_AZ
    while True:
        noise = random.gauss(1., 0.02)
        msg = {
            'timestamp': int(time.time()),
            'R': (R * noise).tolist(),
            'az': (az * noise).tolist(),
            'addrs': [0, 1, 2, 3]
        }
        yield msg
        time.sleep(interval)


def main():
    rd = redis.StrictRedis()
    for msg in next_point(30):
        rd.publish('data.ranges', json.dumps(msg))


if __name__ == '__main__':
    main()
