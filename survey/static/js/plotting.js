/**
 * Plotting functions for Range Survey App
 */
define(["jquery", "hchart"], function($, hc) {
    function plot_initialize(id) {
        var options = {
            chart: {
                renderTo: id
            },

            title: {
                text: 'Tracking Range Layout'
            },

            xAxis: {
                min: null,
                max: null
            },

            yAxis: {
                min: null,
                max: null
            },

            tooltip: {
                formatter: function() {
                    var y = Number(this.y).toFixed(1);
                    var x = Number(this.x).toFixed(1);
                    return this.point.name + ': ' + x + ',' + y;
                }
            },

            series: [{
                type: 'scatter',
                name: 'Hydrophone Locations',
                data: []
            }]
        };

        return new Highcharts.Chart(options);
    }

    function plot_update(chart, data, scale) {
        $.each(data, function(idx, obj){
            var point = chart.get(obj.name);
            var dp = {
                name: obj.name || obj.id,
                id: obj.id || obj.name,
                x: obj.x/scale,
                y: obj.y/scale
            };

            if (obj.color)
                dp.marker = {fillColor: obj.color};

            if (point) {
                point.update(dp, false);
            } else {
                chart.series[0].addPoint(dp, false);
            }
        });
        chart.redraw();
    }

    return {
        update: plot_update,
        initialize: plot_initialize
    };
});
