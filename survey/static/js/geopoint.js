// Represent a lat-lon position
define(["dmm"], function(Dmm) {
    function GeoPoint(lat, lon) {
        if(typeof lat == "string") {
            this.lat = new Dmm(parseFloat(lat));
        } else {
            this.lat = new Dmm(lat);
        }

        if(typeof lon == "string") {
            this.lon = new Dmm(parseFloat(lon));
        } else {
            this.lon = new Dmm(lon);
        }

        this.hlat = "N";
        this.hlon = "E";

        if(this.lat.isneg()) {
            this.lat = this.lat.abs();
            this.hlat = "S";
        }

        if(this.lon.isneg()) {
            this.lon = this.lon.abs();
            this.hlon = "W";
        }
    }

    GeoPoint.prototype = {
        tostring: function() {
            return this.lat.tostring() + this.hlat + " "
                + this.lon.tostring() + this.hlon;
        },
        togeo: function() {
            return {type: "Point",
                    coordinates: [this.lon.todegrees(), this.lat.todegrees()]};
        }
    };

    return GeoPoint;
});
