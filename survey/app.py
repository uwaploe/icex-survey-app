#!/usr/bin/env python
#
# Flask web-app for ICEX range survey. Most of the work is done
# by the Surveyor gRPC server, this app provides a user interface.
#
from flask import Flask, Response, render_template, \
    make_response, abort
import redis
import time
import simplejson as json
import grpc
from gics import api_pb2, api_pb2_grpc
from google.protobuf import json_format
from .utils import Dmm


# Configuration
# Scale factor to convert meters to us-yards
SCALE = 1. / 0.914401828803658

app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(
    REDIS_HOST='localhost',
    REDIS_PORT=6379,
    REDIS_DB=0,
    DEBUG=False,
    SURVEYOR='127.0.0.1:10001'
))
app.config.from_envvar('SURVEY_SETTINGS', silent=True)

# Map gRPC status codes to HTTP
http_codes = {
    grpc.StatusCode.INTERNAL: 400,
    grpc.StatusCode.UNAUTHENTICATED: 401,
    grpc.StatusCode.PERMISSION_DENIED: 403,
    grpc.StatusCode.UNIMPLEMENTED: 404,
    grpc.StatusCode.UNAVAILABLE: 503
}


def connect_redis():
    """
    :rtype: redis.StrictRedis
    """
    return redis.StrictRedis(host=app.config['REDIS_HOST'],
                             port=app.config['REDIS_PORT'],
                             db=app.config['REDIS_DB'])


def connect_grpc():
    chan = grpc.insecure_channel(app.config['SURVEYOR'])
    return api_pb2_grpc.SurveyorStub(chan)


def message_to_json(obj):
    """
    Serialize a protobuf message object to JSON without the indentation.
    """
    s = json_format.MessageToJson(obj)
    obj = json.loads(s)
    # MessageToJson writes int64 values as strings (??!!). The following
    # code is a work-around for timestamps
    if 'tsec' in obj:
        obj['tsec'] = int(obj['tsec'])
    return json.dumps(obj)


def event_stream(rd, stub):
    """
    Listen for messages on the Redis pub-sub channels data.refs
    and data.ranges and use the information to generate the following
    event messages:

    ranges: ranges in meters between every pair of reference points
    refs: the calculated grid positions of every reference point
    gps: the current origin (Camp) GPS position
    """
    assert isinstance(rd, redis.StrictRedis)
    scale = app.config['SCALE']
    # Lower triangle (LT) of range matrix
    lower_tri = [
        (1, 0),
        (2, 0), (2, 1),
        (3, 0), (3, 1), (3, 2),
        (4, 0), (4, 1), (4, 2), (4, 3)
    ]
    pubsub = rd.pubsub()
    pubsub.subscribe('data.ranges', 'data.refs')
    rm = api_pb2.RangeMatrix()
    pset = api_pb2.PointSet()
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            if msg['channel'] == b'data.ranges':
                json_format.Parse(msg['data'], rm)
                if rm.n >= 5:
                    # Generate names for the ranges between each pair
                    # of points, e.g. P1-P2
                    cell_ids = ['{}-{}'.format(rm.ids[r], rm.ids[c])
                                for r, c in lower_tri]
                    R = [float(x)*0.001*scale for x in rm.ranges]
                    ranges = dict(zip(cell_ids,
                                      [R[i*rm.n + j] for i, j in lower_tri]))
                    yield 'event: ranges\ndata: {0}\n\n'.format(
                        json.dumps(ranges))
                try:
                    xy = stub.SetGrid(api_pb2.Empty())
                except grpc.RpcError:
                    pass
                else:
                    yield 'event: refs\ndata: {0}\n\n'.format(
                        message_to_json(xy))
            elif msg['channel'] == b'data.refs':
                json_format.Parse(msg['data'], pset)
                yield 'event: gps\ndata: {0}\n\n'.format(
                    message_to_json(pset.points['origin']))


def get_ref_points(rd, key, delete=False):
    obj = api_pb2.Survey()
    try:
        if delete:
            json_format.Parse(rd.pop(key), obj)
        else:
            json_format.Parse(rd.lindex(key, -1), obj)
    except Exception:
        app.logger.exception('Get survey points')
    return obj


@app.route('/ref')
@app.route('/ref/<fmt>')
def ref(fmt=None):
    """
    Return the most recent reference point grid positions.

    :param fmt: format of returned data, 'xyz' for the Track
                survey file format. Default is JSON.
    """
    rd = connect_redis()
    key = app.config.get('SURVEY_KEY', 'survey')
    obj = get_ref_points(rd, key)
    if fmt == 'xyz':
        if len(obj.points) < 4:
            abort(404)
        hydros = [None] * 4
        for name, p in obj.points.items():
            idx = int(name[-1])
            hydros[idx-1] = p
        resp = make_response(render_template('survey.xyz',
                             points=hydros))
        resp.headers['Content-Type'] = 'text/plain'
        resp.headers['Content-Disposition'] = 'attachment; filename="survey.xyz"'
    else:
        resp = make_response(message_to_json(obj))
        resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route('/survey', methods=['POST'])
def survey():
    """
    Calculate a new grid survey. Store the grid locations of the reference
    points and the latest Range Matrix. Return the reference points.
    """
    rd = connect_redis()
    stub = connect_grpc()
    try:
        xy = stub.SetGrid(api_pb2.Empty())
        rm = stub.GetRanges(api_pb2.Empty())
    except grpc.RpcError as e:
        abort(http_codes.get(e.code(), 400))
    out = message_to_json(xy)
    rd.rpush(app.config.get('SURVEY_KEY', 'survey'), out)
    rd.rpush(app.config.get('RANGES_KEY', 'ranges'), message_to_json(rm))
    resp = make_response(out)
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route('/')
def index():
    """
    Return the index (top-level) page
    """
    stub = connect_grpc()
    data = {}
    try:
        p = stub.LookupPoint(api_pb2.PointReq(id="origin"))
    except grpc.RpcError:
        data = {}
    else:
        lat = Dmm(float(p.latitude/1.0e7))
        lon = Dmm(float(p.longitude/1.0e7))
        hlat = lat.is_neg() and 'S' or 'N'
        hlon = lon.is_neg() and 'W' or 'E'
        data['lat'] = '{0}{1}'.format(str(abs(lat)), hlat)
        data['lon'] = '{0}{1}'.format(str(abs(lon)), hlon)
        data['timestamp'] = time.strftime('%Y-%m-%d %H:%M:%SZ',
                                          time.gmtime(p.tsec))
    return render_template('index.html', **data)


@app.route('/stream')
def stream():
    """
    Start the Server Sent Events stream.
    """
    rd = connect_redis()
    stub = connect_grpc()
    return Response(event_stream(rd, stub), mimetype='text/event-stream')
