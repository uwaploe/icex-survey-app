FROM python:3-alpine AS build-env
RUN apk add --no-cache make gcc g++ musl-dev python3-dev

RUN mkdir -p /app/survey
WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip --no-cache-dir --trusted-host waves.apl.uw.edu \
  install -r requirements.txt

COPY setup.py setup.py
COPY gunicorn.conf gunicorn.conf
COPY survey /app/survey/

RUN pip install -e .

FROM python:3-alpine
RUN mkdir -p /app/survey
WORKDIR /app
COPY --from=build-env /app /app/
COPY --from=build-env /usr/local/lib/python3.6/site-packages /usr/local/lib/python3.6/site-packages
COPY --from=build-env /usr/local/bin /usr/local/bin
COPY --from=build-env /usr/lib/libstdc* /usr/lib/
COPY --from=build-env /usr/lib/libgcc_s* /usr/lib/

EXPOSE 80
ENV GUNICORN_WORKERS=4 \
    GUNICORN_THREADS=2 \
    GUNICORN_TIMEOUT=30

CMD ["gunicorn", "-c", "/app/gunicorn.conf", "-b", "0.0.0.0:80", "survey.app:app"]