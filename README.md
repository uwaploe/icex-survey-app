# ICEX Grid Survey App

This repository contains a [Flask](http://flask.pocoo.org) based web
application which provides the user-interface for the ICEX grid survey
process.
